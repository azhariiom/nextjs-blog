import NextFederationPlugin from '@module-federation/nextjs-mf';

const remotes = (isServer) => {
  const location = isServer ? "ssr" : "chunks";
  return {};
};

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "**.**.com",
      },
    ],
  },
  transpilePackages : [],
  webpack(config, { isServer }) {
    config.plugins.push(
      new NextFederationPlugin({
        name: "product",
        remotes : {},
        filename: "static/chunks/remoteEntry.js",
        exposes : {},
        extraOptions: {
          exposePages: true,
        }
      })
    );
    return config;
  }
};

export default nextConfig;